﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class PutMyPlayer : MonoBehaviour
{
    [SerializeField] private GameObject _characterPrefab;

    private void Start()
    {
        PhotonNetwork.Instantiate(_characterPrefab.name, Vector3.zero, Quaternion.identity);
    }
}