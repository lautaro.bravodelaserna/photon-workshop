﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Random = UnityEngine.Random;

public class PhotonNetworkManager : MonoBehaviourPunCallbacks
{
    private void Start()
    {
        DontDestroyOnLoad(this);
    }

    public void BTNConnectToServer()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public void OnConnectedToServer()
    {
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("BBBB");

        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("AAAAA");
        PhotonNetwork.CreateRoom(Random.Range(0, 999).ToString(), new RoomOptions(){MaxPlayers = 2});
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("RoomCreated");
    }

    public override void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel(1);
    }
}
