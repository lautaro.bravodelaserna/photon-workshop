using Photon.Pun;
using UnityEngine;

public class Character : MonoBehaviourPun
{
    private PhotonView _view;
    private float _speed;

    private void Start()
    {
        _view = GetComponent<PhotonView>();
    }

    private void Update()
    {
        if (!_view.IsMine)
            return;

        MoveCharacter();
    }

    private void MoveCharacter()
    {
        transform.position +=
            new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * (Time.deltaTime * _speed);
    }
}